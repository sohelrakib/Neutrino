<div class="col-md-4 col-sm-4 col-xs-12 blog-sidebar">

    <?php 

    dynamic_sidebar('ps_right_sidebar1');
    get_search_form(); 
      ?>
     

    


      <div>
    <!--  https://paulund.co.uk/how-to-display-author-bio-with-wordpress -->
    <!-- https://wordpress.stackexchange.com/questions/207947/how-to-display-a-users-bio-not-the-author-or-logged-in-user -->
           <h3 class="about_us text-center">  About Us   </h3>

       <div class="author-bio text-center">
      
            <?php echo get_avatar( get_the_author_meta('email'), '90' ); ?>
            
            <div class="author-info">
                    <p class ="author-description text-center">
                         <?php 

                         // the_author_meta('description');  /1st way 
                         // the_author_description();        /2nd way ,below 3rd and the best

                          $walt_id = 1; // Make sure you have the correct ID here
                          $userdata = get_user_meta( $walt_id );
                          echo $userdata['description'][0];


                           ?>
                    </p>
            </div>
      </div>
   </div>


  <?php dynamic_sidebar('ps_right_sidebar2');?>

    <!-- <div>
       <?php wp_list_categories(); ?>
    </div> -->





       <div class="all-feature-post">
             <h3 class="feature-post text-center"> Featured Posts:</h3>

          <!--    http://smallenvelop.com/how-to-create-featured-posts-in-wordpress/ -->

       <?php
  $args = array(
        'posts_per_page' => 5,
        'meta_key' => 'meta-checkbox',
        'meta_value' => 'yes',
        'ignore_sticky_posts' => 1
    );
    $featured = new WP_Query($args);
 
if ($featured->have_posts()): while($featured->have_posts()): $featured->the_post(); ?>
<h4 class="feature-post-title"><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></h4>
<p class="feature-details">By <a class="author" href="<?php the_author_posts() ?>"><?php the_author(); ?> </a>  On <?php echo get_the_date('F j, Y'); echo " "; comments_number(); ?></p>

<?php if (has_post_thumbnail()) : ?>
 <div class="feature-thumb">
<figure> <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a> </figure>
  </div>
   
<?php

endif;
endwhile; else:
endif;
?>
     </div>
     

    
 

    <div class="all-latest-post">
       <h3 class="latest-post text-center"> Latest Posts </h3>
           
           <?php
             $args = array(
              'posts_per_page' => 5,
              'ignore_sticky_posts' => 1
             );
             $the_query = new WP_Query( $args );

            if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();

                get_template_part( 'content/content-sidebar', get_post_format() );
                ?>
                <hr>
                <?php

            endwhile; endif;
            ?>
    </div>



   <div class="banner_add">
    <img src="<?php echo get_template_directory_uri();?>/image/banner_add.jpg" alt="banner_add" width="100%" height=""/>

  </div>



    <?php dynamic_sidebar('ps_right_sidebar3');?>




   <div class="all-most-viewed">

       <h3 class="most-viewed-post text-center">Most Viewed</h3>
<ul>
    <?php $popular = new WP_Query(array('posts_per_page'=>5, 'meta_key'=>'popular_posts', 'orderby'=>'comment_count', 'order'=>'DESC','ignore_sticky_posts' => 1));

    if ( $popular->have_posts() ) :  
    while ($popular->have_posts()) : $popular->the_post(); ?>
   
       <h4 class="most-viewed-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
<li>
    <p class="most-viewed-details">BY <a class="author" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">  <?php the_author(); ?>  </a> ON <?php the_time( get_option( 'date_format' ) );  echo " "; comments_number(); ?> </p>
 
</li>
   <hr>
    <?php endwhile; wp_reset_postdata(); 
     endif; ?>
</ul>
<!--  https://digwp.com/2016/03/diy-popular-posts/ -->
   </div>




  <?php dynamic_sidebar('ps_right_sidebar4');?>

</div><!-- /.blog-sidebar -->