<?php
add_theme_support( 'title-tag' );

// Support Featured Images
add_theme_support( 'post-thumbnails' );
add_theme_support( 'custom-logo' );
add_theme_support('custom-header');
add_theme_support('custom-background');
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'image', 'video', 'link', 'quote' ,'status' , 'audio' ,'chat') );

load_theme_textdomain('neutrino',get_template_directory_uri().'/languages');
//register_nav_menu('psmainmenu',__('main menu','neutrino'));


register_nav_menus(array(
    'primary'=>__('Primary Menu'),
    'footer'=>__('Footer Menu')
));



// Set the default content width.
    // $GLOBALS['content_width'] = 625;










//add awasomefonts 
add_action( 'wp_enqueue_scripts', 'enqueue_load_fa' );
function enqueue_load_fa() {
 
    wp_enqueue_style( 'load-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );
 
}

// Add scripts and stylesheets
function startwordpress_scripts() {

    wp_enqueue_script( 'comment-reply' );
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.6' );
    wp_enqueue_style( 'bootstrap2', get_template_directory_uri() . '/css/font-awesome.min.css' );
    wp_enqueue_style( 'style2', get_template_directory_uri() . '/css/style2.css' );
    wp_enqueue_style( 'carousel-css', get_template_directory_uri() . '/css/owl.carousel.css' );
    wp_enqueue_style( 'theme-css', get_template_directory_uri() . '/css/owl.theme.css' );
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '3.3.6', true );
     wp_enqueue_script( 'owl-js', get_template_directory_uri() . '/js/owl.carousel.js' );
      wp_enqueue_script( 'owl-min-js', get_template_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery' ), '3.3.6', true );
}

add_action( 'wp_enqueue_scripts', 'startwordpress_scripts' );


 //Add Google Fonts
function startwordpress_google_fonts() {
    wp_register_style('Karla', '<link href="https://fonts.googleapis.com/css?family=Karla:700" rel="stylesheet"> ');
    wp_enqueue_style( 'Karla');


    wp_register_style('Karla', '<link href="https://fonts.googleapis.com/css?family=Karla:700" rel="stylesheet"> ');
    wp_enqueue_style( 'Karla');
}

add_action('wp_print_styles', 'startwordpress_google_fonts');

// Changing excerpt more
function new_excerpt_more($more) {
    global $post;
    return '… <a href="'. get_permalink($post->ID) . '">' . 'Read More &raquo;' . '</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');


function my_widget()
{
   register_sidebar(array(

     'name'=> 'ps right sidebar',
     'description'=> 'add right sidebar widget here',
     'id'=>'ps_right_sidebar'
    ));
}
add_action('widgets_init','my_widget');




function my_widget2()
{
   register_sidebar(array(

     'name'=> 'ps right sidebar2',
     'description'=> 'add right sidebar widget here 2',
     'id'=>'ps_right_sidebar2'
    ));
}
add_action('widgets_init','my_widget2');

function my_widget3()
{
   register_sidebar(array(

     'name'=> 'ps right sidebar3',
     'description'=> 'add right sidebar widget here 3',
     'id'=>'ps_right_sidebar3'
    ));
}
add_action('widgets_init','my_widget3');


function my_widget4()
{
   register_sidebar(array(

     'name'=> 'ps right sidebar4',
     'description'=> 'add right sidebar widget here 4',
     'id'=>'ps_right_sidebar4'
    ));
}
add_action('widgets_init','my_widget4');



function my_widget5()
{
   register_sidebar(array(

     'name'=> 'ps footer',
     'description'=> 'add footer widget here 5',
     'id'=>'ps_footer_sidebar5'
    ));
}
add_action('widgets_init','my_widget5');



function my_widget6()
{
   register_sidebar(array(

     'name'=> 'ps header',
     'description'=> 'add header widget here 6',
     'id'=>'ps_header_sidebar6'
    ));
}
add_action('widgets_init','my_widget6');



function my_widget7()
{
   register_sidebar(array(

     'name'=> 'ps index (8 col)',
     'description'=> 'add index widget here 7',
     'id'=>'ps_index_sidebar7'
    ));
}
add_action('widgets_init','my_widget7');


function my_widget8()
{
   register_sidebar(array(

     'name'=> 'ps index (12 col)',
     'description'=> 'add index widget here 8',
     'id'=>'ps_index_sidebar8'
    ));
}
add_action('widgets_init','my_widget8');










function my_new_contactmethods( $contactmethods ) {
    // Add Twitter
    $contactmethods['twitter'] = 'Twitter';
    //add Facebook
    $contactmethods['facebook'] = 'Facebook';

    //add Facebook
    $contactmethods['pinterest'] = 'Pinterest';

    //add Facebook
    $contactmethods['youtube'] = 'Youtube';

    $contactmethods['instagram'] = 'Instagram';


    return $contactmethods;
}
add_filter('user_contactmethods','my_new_contactmethods',10,1);


 function m1_customize_register( $wp_customize ) {

   $wp_customize->add_setting( 'm1_logo' ); // Add setting for logo uploader



   // Add control for logo uploader (actual uploader)

   $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'm1_logo', array(

       'label'    => __( 'Upload 2nd Logo (replaces text)', 'm1' ),

       'section'  => 'title_tagline',

       'settings' => 'm1_logo',

   ) ) );

}

add_action( 'customize_register', 'm1_customize_register' );








require get_template_directory()."/inc/pagination.php";
require get_template_directory()."/inc/subscribe_widget.php";
require get_template_directory()."/inc/feature_post.php";
require get_template_directory()."/inc/favorite-post.php";
require get_template_directory()."/inc/template-tags.php";
require get_template_directory()."/inc/helper.php";






class slide_widget extends WP_Widget {

    public function __construct() {
            $widget_options = array( 
              'classname' => 'slide_widget',
              'description' => 'This is a slide Widget',
            );
            parent::__construct( 'slide_widget', 'Slide Widget', $widget_options );
                                  }



    public function widget( $args, $instance ) {
       
       ?>
        

              <div id="owl-demo">
          
<!--   <div class="item"><img src="<?php echo get_template_directory_uri();?>/slide/a1.jpg" alt="Owl Image"></div>
  <div class="item"><img src="<?php echo get_template_directory_uri();?>/slide/a2.jpg" alt="Owl Image"></div>
  <div class="item"><img src="<?php echo get_template_directory_uri();?>/slide/a3.jpg" alt="Owl Image"></div>
  <div class="item"><img src="<?php echo get_template_directory_uri();?>/slide/a4.jpg" alt="Owl Image"></div>
  <div class="item"><img src="<?php echo get_template_directory_uri();?>/slide/a5.jpg" alt="Owl Image"></div>
  <div class="item"><img src="<?php echo get_template_directory_uri();?>/slide/a6.jpg" alt="Owl Image"></div>
  <div class="item"><img src="<?php echo get_template_directory_uri();?>/slide/a7.jpg" alt="Owl Image"></div> -->

     <?php
             $args = array(
              'posts_per_page' => -1,
              'ignore_sticky_posts' => 1,
              'post_type' => 'post'
             );
             $the_query = new WP_Query( $args );

            if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();

                


                 if ( has_post_thumbnail() ) { ?>

<!-- <div class="item"> -->
 
  
   <div class="slider-thumbnail-image">
        <?php
        
            the_post_thumbnail();

            ?>
            <div class="slide-absolute text-center">
              
           <h4> <?php the_title();?> </h4>

                 <div class="text-center">
    <a class="text-center read_more"href="<?php the_permalink(); ?>"> Read More </a> 
                 </div>
            </div>

        
      </div>

   

  
    
 
<!-- </div>/ -->


                                            <?php
                                            }
                                            
               

            endwhile; endif;
            wp_reset_query();
            ?>
 
     </div>


       <?php
                                                 }



    public function form( $instance ) {
          $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
         
           ?>
          <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
            <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>" />
            
              </p>
                                              <?php
                                                } 

          

           

                                       



   public function update( $new_instance, $old_instance ) {
          $instance = $old_instance;

          $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
         
          return $instance;
                                                           }
                                            }





function register_slide_widget() { 
  register_widget( 'slide_widget' );
}
add_action( 'widgets_init', 'register_slide_widget' );





