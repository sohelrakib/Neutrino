<?php

// a widget for url / subscribe

class subscribe_blog extends WP_Widget {

    public function __construct() {
            $widget_options = array( 
              'classname' => 'blog_subscribe_widget',
              'description' => 'This is a Blog Subscribe Widget',
            );
            parent::__construct( 'blog_widget', 'Blog Widget', $widget_options );
                                  }



    public function widget( $args, $instance ) {
       
         if(!empty($instance['title'])){
       ?>
        <h4 class="text-center widget-subscribe">title: <?php echo $instance['title']; ?> </h4>

        <?php     
                                        }
                if(!empty($instance['message'])){                        
          ?>                               
        <p class="text-center widget-message">message:  <?php echo $instance['message']; ?> </p>

          <?php  } ?>          

        <form action="<?php echo $instance['url']; ?>" method="post">
                  <div class="form-group">
                    <input type="email" class="form-control" id="email" placeholder="EMAIL ADDRESS" required>
                  </div>
                <div class="text-center">
             <button type="submit" class="btn btn-primary text-center subscribe-btn ">subscribe</button>
                </div>
            </form>
       <?php
                                                 }



    public function form( $instance ) {
          $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
          $url = ! empty( $instance['url'] ) ? $instance['url'] : '';
          $message = ! empty( $instance['message'] ) ? $instance['message'] : '';
           ?>
          <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
            <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>" />
            
            <br><br>

            <label for="<?php echo $this->get_field_id( 'url' ); ?>">Url:</label>
            <input class="widefat" type="text" id="<?php echo $this->get_field_id( 'url' ); ?>" name="<?php echo $this->get_field_name( 'url' ); ?>" value="<?php echo esc_attr( $url ); ?>" />

             <br><br>

           <?php if( current_user_can('administrator')) { ?> 

            <label for="<?php echo $this->get_field_id('message'); ?>">message: </label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('message'); ?>" name="<?php echo $this->get_field_name('message'); ?>"><?php echo esc_attr($message); ?></textarea>

                                                  <?php } ?> 

           </p>

           

          <?php 
                                      }     



   public function update( $new_instance, $old_instance ) {
          $instance = $old_instance;

          $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
          $instance[ 'url' ] = strip_tags( $new_instance[ 'url' ] );
          $instance[ 'message' ] = strip_tags( $new_instance[ 'message' ] );
          return $instance;
                                                           }
                                            }





function register_subscribe_blog_widget() { 
  register_widget( 'subscribe_blog' );
}
add_action( 'widgets_init', 'register_subscribe_blog_widget' );