<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title><?php echo get_bloginfo( 'name' ); ?></title>
    <link href="<?php echo get_bloginfo( 'template_directory' );?>/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/owl.carousel.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/owl.theme.css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head();?>

</head>

<body <?php body_class() ?> >


<script>
jQuery(document).ready(function($){
       var offset = 100;
       var speed = 600;
       var duration = 500;
       $(window).scroll(function(){
           if ($(this).scrollTop() < offset) {
               $('.topbutton') .fadeOut(duration);
           } else {
               $('.topbutton') .fadeIn(duration);
           }
       });
       $('.topbutton').on('click', function(){
           $('html, body').animate({scrollTop:0}, speed);
           return false;
       });
   });


   
</script>


<a  class="topbutton" href="">
   <i class="fa fa-chevron-up" aria-hidden="true"></i>
</a>

<!-- making custom menu -->
<div class="container-fluid">
    <div class="row header-wrap">

        <div class="col-md-3 logo">
            <?php
            $custom_logo_id = get_theme_mod( 'custom_logo' );
            $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
            ?>
            <a href="<?php echo get_home_url(); ?>">
                <img class="site-logo" src="<?php echo $image[0]; ?>" alt="not found" >
                 
            </a>
        </div>



        <div class="col-md-7">
            <div id="bar">
                <?php
                $args=array(
                    'theme_location'=>'primary'
                );
                ?>
                <?php wp_nav_menu($args); ?>
            </div>
        </div>


        <div class="col-md-2 social_icon_wrap">
            <?php
            $walt_id = 1; // Make sure you have the correct ID here
            $userdata = get_user_meta( $walt_id );

            if(!empty($userdata['facebook'][0])){
            ?>

            <a href="<?php  echo $userdata['facebook'][0]; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i> </a>
            
            <?php 
             } 
            
             if(!empty($userdata['twitter'][0])){
             ?>

            <a href="<?php  echo $userdata['twitter'][0]; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
           
             <?php 
               } 
            
             if(!empty($userdata['pinterest'][0])){
              ?>
            <a href="<?php  echo $userdata['pinterest'][0]; ?>" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a>

             <?php 
               } 
            
             if(!empty($userdata['youtube'][0])){
              ?>

            <a href="<?php  echo $userdata['youtube'][0]; ?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>

             <?php 
               } 
            
             if(!empty($userdata['instagram'][0])){
              ?>

            <a href="<?php  echo $userdata['instagram'][0]; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
       

              <?php 
               } 
        
              ?>

              <span class="navbar-text">
  <i class="fa fa-search" aria-hidden="true"></i>
</span>


           <div class="search-barh">
                <?php get_search_form(); ?>
            </div><!--  .search-bar -->


           <script>


               jQuery(document).ready(function($){
                    $('.fa-search').on('click', function() {
                        $('.search-barh').slideToggle();
                    });
                });

           </script>


        </div>




    </div>


      

      <div class="container">
              <?php dynamic_sidebar('ps_header_sidebar6');?>
      </div>


    

</div>



<!-- <?php echo get_home_url(); ?> -->
<div class="container">