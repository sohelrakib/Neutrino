</div> <!-- /.container -->

   


   <div class="container">
              <?php dynamic_sidebar('ps_footer_sidebar5');?>
      </div>


<footer class="blog-footer">
    <div class="container-fluid">
<div class="row footer-wrap">
   
  


    <div class="col-md-2 logo">

        <?php if ( get_theme_mod( 'm1_logo' ) ) : ?>

            <a href="<?php echo get_home_url(); ?>" id="site-logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">



                <img src="<?php echo get_theme_mod( 'm1_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">



            </a>



        <?php else : ?>



            <hgroup>

                <h1 class="site-title"><a href="<?php echo get_home_url(); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>

                <p class="site-description"><?php bloginfo( 'description' ); ?></p>

            </hgroup>



        <?php endif; ?>

    </div>



     <div class="col-md-7">
       <div id="footer-bar">
           <?php
           $args=array(
               'theme_location'=>'footer'
           );
           ?>
           <?php wp_nav_menu($args); ?>
      </div>
    </div>


    <div class="col-md-2 social_icon_wrap">
         <?php
            $walt_id = 1; // Make sure you have the correct ID here
            $userdata = get_user_meta( $walt_id );

            if(!empty($userdata['facebook'][0])){
            ?>

            <a href="<?php  echo $userdata['facebook'][0]; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i> </a>
            
            <?php 
             } 
            
             if(!empty($userdata['twitter'][0])){
             ?>

            <a href="<?php  echo $userdata['twitter'][0]; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i> </a>
           
             <?php 
               } 
            
             if(!empty($userdata['pinterest'][0])){
              ?>
            <a href="<?php  echo $userdata['pinterest'][0]; ?>" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a>

             <?php 
               } 
            
             if(!empty($userdata['youtube'][0])){
              ?>

            <a href="<?php  echo $userdata['youtube'][0]; ?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>

             <?php 
               } 
            
             if(!empty($userdata['instagram'][0])){
              ?>

            <a href="<?php  echo $userdata['instagram'][0]; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
       

              <?php 
               } 
        
              ?>



    </div>
</div>
</div>
  

    

   

    <!-- <p>
        <a href="#">Back to top</a>
    </p> -->
</footer>

<script src="<?php echo get_template_directory_uri();?>/js/owl.carousel.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/owl.carousel.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/post-like.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>


    $(document).ready(function() {
     
      $("#owl-demo").owlCarousel({
     
          autoPlay: 3000, //Set AutoPlay to 3 seconds
     
          items : 4,
          itemsDesktop : [1199,3],
          itemsDesktopSmall : [979,3]
     
      });
     
    });


</script>
<?php wp_footer(); ?>
</body>
</html>