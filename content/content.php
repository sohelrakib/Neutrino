<div class="blog-post">
    <div class="single-page-format">


   <h2 class="page-title text-center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <p class="blog-post-meta text-center"><?php the_time( get_option( 'date_format' ) ); ?> by <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">  <?php the_author(); ?>  </a></p>


<div class="page-thumbnail text-center img-responsive">
   <?php
    if ( has_post_thumbnail() ) {
                the_post_thumbnail('medium_large');
            }
    ?>

</div>

 <div class="img-responsive">
    <?php the_content(); ?>
  </div>

</div>
</div><!-- /.blog-post -->