<div class="blog-post">
    <div class="link-content img-responsive">
 
    <h2 class="link-title text-center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

      <?php the_content(); ?>
    
     <p class="link-post text-center">by <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">  <?php the_author(); ?>  </a> on <?php the_time( get_option( 'date_format' ) ); ?>/ <?php the_category(', '); ?>  <?php comments_number(); ?> </p>
         

     </div>
</div><!-- /.blog-post -->