<div class="blog-post">
   <div class="default-content img-responsive">

   <?php
    if ( has_post_thumbnail() ) {
    the_post_thumbnail('medium_large');
    }
    ?>



       <p class="default-post text-center">by <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">  <?php the_author(); ?>  </a> on <?php the_time( get_option( 'date_format' ) ); ?>/<span class="author-catregorized"> <?php the_category(', '); ?></span> <?php comments_number(); ?> </p>

 
        <h2 class="default-title text-center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

   



   <?php the_content(); ?>
    
   </div>
</div><!-- /.blog-post -->