<div class="blog-post">

 

  
  <div class="quote-content img-responsive">

        <p class="quote-post">by <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">  <?php the_author(); ?>  </a> on <span class="time-catagory"> <?php the_time( get_option( 'date_format' ) ); ?> /</span> <?php the_category(', '); ?>  <?php comments_number(); ?> </p>

   
        <h2 class="quote-post-content"> <q> <a href="<?php the_permalink(); ?>">  <?php the_content(); ?>  </a> </q> </h2>

   
        <p class="quote-title"> - <?php the_title(); ?> </p>

   </div>


</div><!-- /.blog-post -->


