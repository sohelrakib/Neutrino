<div class="blog-post">
   <div class="audio-content img-responsive">
  
      <?php 
   
      the_content(); ?>


        <p class="audio-post text-center">by <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">  <?php the_author(); ?>  </a> on <span class="time-catagory"> <?php the_time( get_option( 'date_format' ) ); ?> /</span> <?php the_category(', '); ?>  <?php comments_number(); ?> </p>


    

    
        <h2 class="audio-title text-center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
       <!--  <?php echo get_permalink( $post->ID ); ?> -->

    
  </div>
</div><!-- /.blog-post -->
