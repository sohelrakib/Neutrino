<div class="blog-post">
     <div class="gallery-content img-responsive">
   
 

        <p class="gallery-post text-center">by <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">  <?php the_author(); ?>  </a> on <?php the_time( get_option( 'date_format' ) ); ?>/ <?php the_category(', '); ?>  <?php comments_number(); ?> </p>

  
       <h2 class="gallery-title text-center">   <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>   </h2>


       <div class="image-gallery img-responsive">
         <?php the_content(); ?>
       </div> 


    
  

     </div>
</div><!-- /.blog-post -->

