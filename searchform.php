

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<div class="search-field-relative text-center">
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	
	<input type="search" id="<?php echo $unique_id; ?>" class="search-field" placeholder="<?php echo esc_attr_x( 'Search Post ', 'Search Posts', '
	neutrino' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	<div class="ser-icon-abs"><span class="fa fa-search errspan"></span></div>

</form>

 </div>